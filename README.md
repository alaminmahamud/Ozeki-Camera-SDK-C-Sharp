#Working With Ozeki Camera SDK C Sharp
**Description** - this repository will discuss working with camera in C# using .NET methodologies
![ozeki-logo](http://www.camera-sdk.com/attachments/29/how_ozeki_ip_camera_sdk_works.jpg)

#Tools
- Visual Studio
- DotNet Framework
- Ozeki Camera SDK

#ONVIF
![onvif-logo](http://www.karel.com.tr/sites/default/files/cctv-onvif-560.jpg)
<br/>
ONVIF is a global and open industry forum with the goal to facilitate the development and use of a global open standard for the interface of physical IP-based security products

ONVIF :: open network video interface forum

#ONVIF Standardization 
onvif standardization covers the area below

1. Device Discovery
2. Device Management
3. Alarm Handling
4. PTZ [pan tilt zoom] control
5. Video Analytics
6. Security

#Onvif benefits

1. interoperability
2. flexibility
3. future proof
4. quality

#Difference Between Cameras

1.USB Camera
2.RTSP Camera
3.Onvif ip Camera

##USB Camera
![usb camera](http://g04.s.alicdn.com/kf/HT1UPzJFTRcXXagOFbX0/205825886/HT1UPzJFTRcXXagOFbX0.jpg)
<br/>
1. USB Cable Connection to LAN
2. 1 Stream
3. Functionality Limited - **Image** And **Audio** Sending

####USB Cable
![usb cable](http://i.stack.imgur.com/83oKl.png)

##RTSP Camera
![rtsp camera](http://www.camera-sdk.com/attachments/80/rtsp_camera_rtp_streaming.jpg)
<br/>
1. UTP connection to LAN
2. 1 Stream
3. Functionality Limited - **Image** And **Audio** Sending

####UTP Connection
![utp cable](http://www.dlinkmea.com/partner/media/product_images/image_resized/400_2807340-NCB-5EUBLUR-100.png)

##Onvif ip camera
1. UTP cable connection to LAN
2. multi stream
3. More Functionalities : PTZ[pan-tilt-zoom], image setting et cetera


